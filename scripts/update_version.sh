#!/usr/bin/env bash

version=0.2.6
versionlabel="v"${version}

echo "Updating version to ${versionlabel} in setup.py"
match="s/version = '.*'/version = '""${version}""'/g"
sed -i "" -E "${match}" setup.py

echo "Creating and pushing commit for release ${versionlabel}"
git add setup.py scripts/update_version.sh; git commit -m "Release version ${versionlabel}"
git push bitbucket master

echo "Creating and pushing tag for release ${versionlabel}"
git tag -d ${versionlabel}
git push bitbucket :refs/tags/${versionlabel}
git tag -a ${versionlabel} -m "${versionlabel}"
git push bitbucket --tags
