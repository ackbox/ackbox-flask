#!/usr/bin/env bash

# Get action type
action=$1

# Generate CERT
sudo certbot certonly --manual --email contact@ackbox.com \
    -d ackbox.com \
    -d www.ackbox.com \
    -d refs.ackbox.com

#    -d xpenser.ackbox.com \
#    -d news.ackbox.com \
#    -d dash.ackbox.com \
#    -d pynder.ackbox.com \
#    -d vault.ackbox.com \
#    -d subs.ackbox.com \
#    -d mail.ackbox.com

# Add/update Heroku CERT
if [ "${action}" = "add" ]; then
    echo "Adding Heorku certifications"
    #sudo heroku certs:add /etc/letsencrypt/live/ackbox.com/fullchain.pem /etc/letsencrypt/live/ackbox.com/privkey.pem --confirm ackbox
    #sudo heroku _certs:add /etc/letsencrypt/live/ackbox.com/fullchain.pem /etc/letsencrypt/live/ackbox.com/privkey.pem --app ackbox
    #sudo heroku _certs:add /etc/letsencrypt/live/ackbox.com/fullchain.pem /etc/letsencrypt/live/ackbox.com/privkey.pem --app ackbox-refs
else
    echo "Updating Heorku certifications"
    #sudo heroku certs:update /etc/letsencrypt/live/ackbox.com/fullchain.pem /etc/letsencrypt/live/ackbox.com/privkey.pem --confirm ackbox
    #sudo heroku _certs:update /etc/letsencrypt/live/ackbox.com/fullchain.pem /etc/letsencrypt/live/ackbox.com/privkey.pem --app ackbox
    #sudo heroku _certs:update /etc/letsencrypt/live/ackbox.com/fullchain.pem /etc/letsencrypt/live/ackbox.com/privkey.pem --app ackbox-refs
fi
