#!/usr/bin/env bash
virtualenv ~/.virtualenvs/env-ackbox-flask
source ~/.virtualenvs/env-ackbox-flask/bin/activate
pip3 install -r requirements.txt
