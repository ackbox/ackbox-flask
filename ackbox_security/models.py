from datetime import datetime

ROLE_USER = 'user'
ROLE_ADMIN = 'admin'


class User(object):
    def __init__(self, user_id, email, username, password, is_active=False, roles=None, created_at=None):
        self.user_id = user_id
        self.email = email
        self.username = username
        self.password = password
        self.is_active = is_active
        self.roles = roles
        self.created_at = created_at or datetime.utcnow()

    @property
    def is_admin(self):
        return ROLE_ADMIN in self.roles

    @property
    def is_authenticated(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.user_id)

    @classmethod
    def new_for_registration(cls, user_id, email, username, password, roles=None):
        return cls(
            user_id,
            email,
            username,
            password,
            is_active=False,
            roles=roles or [ROLE_USER],
            created_at=datetime.utcnow()
        )

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return '<User {}:{}>'.format(self.username, self.email)

