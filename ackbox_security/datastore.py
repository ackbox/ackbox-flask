class SecurityDatastore(object):
    def get_user_by_id(self, user_id):
        raise NotImplemented()

    def get_user_by_email(self, email):
        raise NotImplemented()

    def get_user_by_username(self, username):
        raise NotImplemented()

    def is_username_unique(self, username):
        raise NotImplemented()

    def is_username_email(self, email):
        raise NotImplemented()

    def save_user(self, user):
        raise NotImplemented()
