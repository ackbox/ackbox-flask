import uuid

from flask import current_app
from itsdangerous import URLSafeTimedSerializer

from ackbox_security.exceptions import *
from ackbox_security.models import User


class SecurityService(object):
    _ACCOUNT_CONFIRM_KEY = 'account-confirm-key'
    _ACCOUNT_RECOVER_KEY = 'account-recover-key'

    def __init__(self, datastore, bcrypt):
        self._datastore = datastore
        self._bcrypt = bcrypt

    def get_user_with_id(self, user_id):
        return self._datastore.get_user_by_id(user_id)

    def is_email_registered(self, email):
        return self._datastore.get_user_by_email(email) is not None

    def validate_credentials(self, username, plaintext_password):
        user = self._datastore.get_user_by_username(username)
        if not user:
            raise InvalidUsernameException(username)
        if not self._do_passwords_match(user.password.encode('utf8'), plaintext_password):
            raise InvalidPasswordException(plaintext_password)
        if not user.is_active:
            raise InactiveUserException(user.email)
        return user

    def update_password(self, email, plaintext_password):
        user = self._datastore.get_user_by_email(email)
        if not user:
            raise InvalidEmailException(email)
        user.password = self._encrypt_password(plaintext_password)
        self._datastore.save_user(user)
        return user

    def change_password(self, username, plaintext_old_password, plaintext_new_password):
        user = self.validate_credentials(username, plaintext_old_password)
        user.password = self._encrypt_password(plaintext_new_password)
        self._datastore.save_user(user)
        return user

    def activate_user(self, email):
        user = self._datastore.get_user_by_email(email)
        user.is_active = True
        self._datastore.save_user(user)
        return user

    def register_user(self, username, plaintext_password, email, roles=None):
        if not self._datastore.is_username_unique(username):
            raise UsernameNotUniqueException(username)
        if not self._datastore.is_email_unique(email):
            raise EmailNotUniqueException(email)
        encrypted_password = self._encrypt_password(plaintext_password)
        user = User.new_for_registration(uuid.uuid4().hex, email, username, encrypted_password, roles=roles)
        self._datastore.save_user(user)
        return user

    def encode_account_confirmation_email(self, email):
        return self._encode_email(email, salt=self._ACCOUNT_CONFIRM_KEY)

    def decode_account_confirmation_email(self, token):
        return self._decode_email(token, salt=self._ACCOUNT_CONFIRM_KEY)

    def encode_account_recovery_email(self, email):
        return self._encode_email(email, salt=self._ACCOUNT_RECOVER_KEY)

    def decode_account_recovery_email(self, token):
        return self._decode_email(token, salt=self._ACCOUNT_RECOVER_KEY)

    def _do_passwords_match(self, encrypted_password, plaintext_password):
        return self._bcrypt.check_password_hash(encrypted_password, plaintext_password)

    def _encrypt_password(self, plaintext_password):
        return self._bcrypt.generate_password_hash(plaintext_password).decode('utf8')

    @staticmethod
    def _decode_email(token, salt):
        try:
            return URLSafeTimedSerializer(current_app.config['SECRET_KEY']).loads(token, salt=salt, max_age=86400)
        except:
            raise InvalidTokenException(token)

    @staticmethod
    def _encode_email(email, salt):
        return URLSafeTimedSerializer(current_app.config['SECRET_KEY']).dumps(email, salt=salt)
