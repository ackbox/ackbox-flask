from functools import wraps

from flask import current_app
from flask_login import current_user

from ackbox_security import login_manager


def requires_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            for user_role in current_user.roles:
                if user_role in roles:
                    return f(*args, **kwargs)
            return login_manager.unauthorized()

        return wrapped

    return wrapper


def requires_login(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.is_authenticated:
            return current_app.login_manager.unauthorized()
        return func(*args, **kwargs)

    return decorated_view


def validate_roles(required_roles):
    for user_role in current_user.roles:
        if user_role in required_roles:
            return True
    return login_manager.unauthorized()


def is_user_authorized(required_roles):
    if not is_user_authenticated():
        return False

    for user_role in current_user.roles:
        if user_role in required_roles:
            return True
    return False


def is_user_authenticated():
    return current_user.is_authenticated
