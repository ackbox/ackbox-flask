class AuthenticationException(Exception):
    def __repr__(self):
        return '<{} {!r}>'.format(self.__class__.__name__, self.__dict__)

    def __str__(self):
        return self.__repr__()


class InactiveUserException(AuthenticationException):
    def __init__(self, user_email):
        self.user_email = user_email


class InvalidEmailException(AuthenticationException):
    def __init__(self, email):
        self.email = email


class InvalidUsernameException(AuthenticationException):
    def __init__(self, username):
        self.username = username


class InvalidPasswordException(AuthenticationException):
    def __init__(self, plaintext_password):
        self.plaintext_password = plaintext_password


class InvalidTokenException(AuthenticationException):
    def __init__(self, token):
        self.token = token


class UsernameNotUniqueException(Exception):
    def __init__(self, username):
        self.username = username


class EmailNotUniqueException(Exception):
    def __init__(self, email):
        self.email = email
