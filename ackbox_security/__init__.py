from ackbox_security.service import SecurityService
from flask import g
from flask import current_app
from flask_sslify import SSLify
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_login import current_user as flask_login_current_user
from flask_mail import Mail
from werkzeug.local import LocalProxy

mail = Mail()
login_manager = LoginManager()
bcrypt = Bcrypt()
current_user = flask_login_current_user


def build_datastore():
    return current_app.config['ACKBOX_SECURITY_DATASTORE_FACTORY'].datastore()


service = SecurityService(LocalProxy(lambda: build_datastore()), bcrypt)


class Security(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app, url_prefix=None, register_view=True):
        if app.config['ACKBOX_USE_SSL']:
            SSLify(app, permanent=True)
        bcrypt.init_app(app)
        mail.init_app(app)
        login_manager.init_app(app)
        self.init(app, url_prefix=url_prefix, register_view=register_view)

    def init(self, app, url_prefix, register_view):
        def before_request():
            g.user = current_user

        with app.app_context():
            from ackbox_security import module
            app.before_request(before_request)
            if register_view:
                app.register_blueprint(module.blueprint, url_prefix=url_prefix)
                login_manager.login_view = 'ackbox_security.login'
