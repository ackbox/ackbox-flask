from ackbox_persistence.models import Record
from ackbox_persistence.models import exceptions
from ackbox_persistence.models import fields
from ackbox_security.datastore import SecurityDatastore
from ackbox_security.models import User


class MongoEngineSecurityDatastore(SecurityDatastore):
    def __init__(self, user_class):
        self._user_class = user_class

    def get_user_by_id(self, user_id):
        user = self._user_class.get_by_id(user_id)
        if user:
            return self._to_user(user)
        return None

    def get_user_by_email(self, email):
        user = self._user_class.get_by_email(email)
        if user:
            return self._to_user(user)
        return None

    def get_user_by_username(self, username):
        user = self._user_class.get_by_username(username)
        if user:
            return self._to_user(user)
        return None

    def is_username_unique(self, username):
        return self._user_class.is_username_unique(username)

    def is_email_unique(self, email):
        return self._user_class.is_email_unique(email)

    def save_user(self, user):
        mongoengine_user = self._user_class.get_by_id(user.user_id)
        if mongoengine_user:
            mongoengine_user.update(
                user_id=user.user_id,
                email=user.email,
                username=user.username,
                password=user.password,
                is_active=user.is_active,
                roles=user.roles,
                created_at=user.created_at,
            )
        else:
            self._to_mongo_user(user).save()

    def _to_user(self, user):
        return User(
            user_id=user.user_id,
            email=user.email,
            username=user.username,
            password=user.password,
            is_active=user.is_active,
            roles=user.roles,
            created_at=user.created_at,
        )

    def _to_mongo_user(self, user):
        return self._user_class(
            user_id=user.user_id,
            email=user.email,
            username=user.username,
            password=user.password,
            is_active=user.is_active,
            roles=user.roles,
            created_at=user.created_at,
        )


class MongoEngineUser(Record):
    user_id = fields.StringField(required=True)
    email = fields.EmailField(required=True)
    username = fields.StringField(unique=True, required=True, min_length=5, max_length=30)
    password = fields.StringField(required=True, max_length=255)
    is_active = fields.BooleanField(required=True, default=False)
    roles = fields.ListField(fields.StringField(), required=True)

    meta = {'collection': 'user', 'allow_inheritance': True, 'strict': False}

    @classmethod
    def get_by_id(cls, user_id):
        try:
            return cls.objects.get(user_id=user_id)
        except exceptions.DoesNotExist:
            return None

    @classmethod
    def get_by_username(cls, username):
        try:
            return cls.objects.get(username=username)
        except exceptions.DoesNotExist:
            return None

    @classmethod
    def get_by_email(cls, email):
        try:
            return cls.objects.get(email=email)
        except exceptions.DoesNotExist:
            return None

    @classmethod
    def is_username_unique(cls, username):
        return cls.objects(username=username).count() == 0

    @classmethod
    def is_email_unique(cls, email):
        return cls.objects(email=email).count() == 0

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return '<User {}>'.format(self.username)


class MongoEngineSecurityDatastoreFactory(object):
    def __init__(self, user_class=MongoEngineUser):
        self._user_class = user_class

    def datastore(self):
        return MongoEngineSecurityDatastore(user_class=self._user_class)
