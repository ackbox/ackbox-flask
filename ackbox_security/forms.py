from flask_wtf import FlaskForm
from wtforms import BooleanField
from wtforms import PasswordField
from wtforms import StringField
from wtforms import validators


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[
        validators.DataRequired(),
        validators.Length(min=5, max=30),
    ])
    email = StringField('Email Address', validators=[
        validators.DataRequired(),
        validators.Email('This field requires a valid email address'),
    ])
    password = PasswordField('Password', validators=[
        validators.DataRequired(),
        validators.Length(min=8, max=30),
        validators.EqualTo('confirm_password', message='Passwords must match'),
    ])
    confirm_password = PasswordField('Confirm Password', validators=[
        validators.DataRequired(),
        validators.Length(min=8, max=30),
    ])


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[validators.DataRequired()])
    password = PasswordField('Password', validators=[validators.DataRequired()])
    remember_me = BooleanField('Remember Me')


class EmailForm(FlaskForm):
    email = StringField('Email Address', validators=[
        validators.DataRequired(),
        validators.Email('This field requires a valid email address'),
    ])


class PasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[
        validators.DataRequired(),
        validators.Length(min=8, max=30),
        validators.EqualTo('confirm_password', message='Passwords must match'),
    ])
    confirm_password = PasswordField('Confirm Password', validators=[
        validators.DataRequired(),
        validators.Length(min=8, max=30),
    ])


class ChangePasswordForm(FlaskForm):
    old_password = PasswordField('Password', validators=[validators.DataRequired()])
    new_password = PasswordField('New Password', validators=[
        validators.DataRequired(),
        validators.Length(min=8, max=30),
        validators.EqualTo('confirm_new_password', message='Passwords must match'),
    ])
    confirm_new_password = PasswordField('Confirm New Password', validators=[
        validators.DataRequired(),
        validators.Length(min=8, max=30),
    ])
