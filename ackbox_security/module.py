import logging

from flask import Blueprint
from flask import abort
from flask import current_app
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask_login import login_user
from flask_login import logout_user

from ackbox_core.mailing import render_email_template
from ackbox_core.mailing import send_email
from ackbox_security import current_user
from ackbox_security import login_manager
from ackbox_security import service
from ackbox_security.exceptions import *
from ackbox_security.forms import EmailForm
from ackbox_security.forms import LoginForm
from ackbox_security.forms import PasswordForm
from ackbox_security.forms import ChangePasswordForm
from ackbox_security.forms import RegistrationForm
from ackbox_security.validation import requires_login

logger = logging.getLogger(__name__)

blueprint = Blueprint('ackbox_security', __name__, static_url_path='/static/ackbox_security')


@login_manager.user_loader
def load_user(user_id):
    return service.get_user_with_id(user_id)


@blueprint.route('/account/logout')
def logout():
    if current_user.is_authenticated:
        logout_user()
    logout_location = current_app.config.get('ACKBOX_SECURITY_LOGOUT_REDIRECT_URL')
    logger.debug('Redirecting to [%s]', logout_location)
    return redirect(logout_location)


@blueprint.route('/account/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        try:
            user = service.validate_credentials(username, password)
            login_user(user, remember=form.remember_me.data)
            logger.info('User [%s] logged in successfully', user.username)
            flash('Logged in successfully')
            return redirect_to_login_redirect_location()
        except InactiveUserException as e:
            logger.warning('User [%s] is not confirmed yet', username)
            flash('Your account has not been confirmed yet', 'warning')
            return redirect(url_for('ackbox_security.reconfirmation', email=e.user_email))
        except AuthenticationException as e:
            logger.warning('Authentication error for [%s:%s]: %s', username, password, e)
            flash('Invalid username or password', 'warning')
            return render_template('security/login.html', form=form)
    return render_template('security/login.html', form=form)


@blueprint.route('/account/registration', methods=['GET', 'POST'])
def registration():
    if current_user.is_authenticated:
        return redirect_to_login_redirect_location()

    form = RegistrationForm(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        email = form.email.data
        try:
            service.register_user(username, password, email)
            send_confirmation_email(email)
            return render_template('security/confirmation.html', email=email)
        except UsernameNotUniqueException:
            form.username.errors.append('Username {} is already taken'.format(username))
            return render_template('security/registration.html', form=form)
        except EmailNotUniqueException:
            form.email.errors.append('Email {} is already registered'.format(email))
            return render_template('security/registration.html', form=form)
    return render_template('security/registration.html', form=form)


@blueprint.route('/account/change', methods=['GET', 'POST'])
@requires_login
def change():
    form = ChangePasswordForm(request.form)
    if form.validate_on_submit():
        username = current_user.username
        old_password = form.old_password.data
        new_password = form.new_password.data
        try:
            service.change_password(username, old_password, new_password)
            logger.info('User [%s] changed password successfully', username)
            flash('Password changed successfully')
            return redirect_to_login_redirect_location()
        except AuthenticationException as e:
            logger.warning('Authentication error for [%s:%s]: %s', username, old_password, e)
            flash('Old password does not match records', 'warning')
            return render_template('security/change.html', form=form)
    return render_template('security/change.html', form=form)


@blueprint.route('/account/reconfirmation', methods=['GET'])
def reconfirmation():
    email = request.args.get('email')
    send_confirmation_email(email)
    flash('Confirmation email has been sent successfully')
    return render_template('security/confirmation.html', email=email)


@blueprint.route('/account/confirmation', methods=['GET'])
def confirmation():
    try:
        email = service.decode_account_confirmation_email(request.args.get('token'))
    except InvalidTokenException:
        abort(404)
        return
    logger.info('Received account confirmation request from [%s]', email)
    service.activate_user(email)
    flash('Account activated successfully')
    return redirect(url_for('ackbox_security.login'))


@blueprint.route('/account/forgotten', methods=['GET', 'POST'])
def forgotten():
    form = EmailForm(request.form)
    if form.validate_on_submit():
        email = form.email.data
        if service.is_email_registered(email):
            send_recovery_email(email)
        flash('If the provided information matches our records, an '
              'email will be sent to you with recovery instructions')
        return redirect('/')
    return render_template('security/forgotten.html', form=form)


@blueprint.route('/account/recovery', methods=['GET', 'POST'])
def recovery():
    try:
        email = service.decode_account_recovery_email(request.args.get('token'))
    except InvalidTokenException:
        abort(404)
        return
    logger.info('Received account recovery request from [%s]', email)
    form = PasswordForm(request.form)
    if form.validate_on_submit():
        service.update_password(email, form.password.data)
        flash('Account recovered successfully')
        return redirect(url_for('ackbox_security.login'))
    return render_template('security/recovery.html', form=form)


@blueprint.route('/account/email_confirmation_test', methods=['GET'])
def email_confirmation_rendering():
    subject = '{} - Account Confirmation'.format(current_app.config['ACKBOX_HTML_SITE_NAME'])
    content = render_template('security/email_confirmation.html')
    return render_email_template(subject, content)


@blueprint.route('/account/email_recovery_test', methods=['GET'])
def email_recovery_rendering():
    subject = '{} - Account Recovery'.format(current_app.config['ACKBOX_HTML_SITE_NAME'])
    content = render_template('security/email_recovery.html')
    return render_email_template(subject, content)


def send_confirmation_email(email):
    subject = '{} - Account Confirmation'.format(current_app.config['ACKBOX_HTML_SITE_NAME'])
    token = service.encode_account_confirmation_email(email)
    confirmation_url = url_for('ackbox_security.confirmation', token=token, _external=True)
    content = render_template('security/email_confirmation.html', confirmation_url=confirmation_url)
    logger.info('Sending email to [%s] with token [%s]', email, token)
    send_email(email, subject, content)


def send_recovery_email(email):
    subject = '{} - Account Recovery'.format(current_app.config['ACKBOX_HTML_SITE_NAME'])
    token = service.encode_account_recovery_email(email)
    recovery_url = url_for('ackbox_security.recovery', token=token, _external=True)
    content = render_template('security/email_recovery.html', recovery_url=recovery_url)
    logger.info('Sending email to [%s] with token [%s]', email, token)
    send_email(email, subject, content)


def redirect_to_login_redirect_location():
    login_redirect_location = current_app.config.get('ACKBOX_SECURITY_LOGIN_REDIRECT_URL')
    logger.debug('Redirecting to [%s]', login_redirect_location)
    return redirect(login_redirect_location)


def redirect_url(default='/'):
    return request.args.get('next') or request.referrer or url_for(default)
