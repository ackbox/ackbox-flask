from ackbox_core.settings import MetaConfig
from ackbox_security.datastore_mongoengine import MongoEngineSecurityDatastoreFactory


class SecurityCoreConfig(MetaConfig):
    ACKBOX_SECURITY_DATASTORE_FACTORY = MongoEngineSecurityDatastoreFactory()
    ACKBOX_SECURITY_LOGIN_REDIRECT_URL = '/'
    ACKBOX_SECURITY_LOGOUT_REDIRECT_URL = '/'
