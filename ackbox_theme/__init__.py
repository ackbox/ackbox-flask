class Theme(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app, url_prefix=None):
        from ackbox_theme import module
        app.register_blueprint(module.blueprint, url_prefix=url_prefix)
