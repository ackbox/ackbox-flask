CORE_CSS_ASSETS = (
    'ackbox_theme/css/vendor/bootstrap.min.css',
    'ackbox_theme/css/style.css',
)

CORE_JS_ASSETS = (
    'ackbox_theme/js/vendor/jquery.min.js',
    'ackbox_theme/js/vendor/bootstrap.min.js',
    'ackbox_theme/js/vendor/moment.js',
    'ackbox_theme/js/script.js',
)
