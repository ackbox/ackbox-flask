from flask import Blueprint

blueprint = Blueprint(
    'ackbox_theme',
    __name__,
    static_url_path='/static/ackbox_theme',
    static_folder='static',
    template_folder='templates')
