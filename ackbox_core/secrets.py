import binascii
import os


def read_or_generate_secret(name, size):
    if not os.path.exists(name):
        secret = binascii.hexlify(os.urandom(size)).decode('utf-8')
        with open(name, mode='w') as f:
            return f.write(secret)

    with open(name, mode='r') as f:
        return f.read()


def get_app_secret():
    return read_or_generate_secret('secrets/.ackbox.app.secret', 24)
