def boundary(excaption_class):
    def inner(function):
        def wrapper(*args, **kwargs):
            try:
                return function(*args, **kwargs)
            except Exception as e:
                raise excaption_class(e)

        return wrapper
    return inner
