from locale import currency


def format_date(value, format='%Y-%m-%d'):
    return value.strftime(format)


def format_currency(value):
    return currency(value, symbol=True, grouping=True)


def format_percentage(value):
    return '%s%%' % value


class Filters(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        app.jinja_env.filters['date'] = format_date
        app.jinja_env.filters['currency'] = format_currency
        app.jinja_env.filters['percentage'] = format_percentage
