import os
import logging as rootlogging

from flask import Flask
from flask import request
from flask import render_template
from flask_wtf import CSRFProtect

from ackbox_core.assets import Assets
from ackbox_core.compression import Compress
from ackbox_core.filters import Filters
from ackbox_core.logging import Logger
from ackbox_core.mailing import Mailer
from ackbox_core.settings import DevConfig
from ackbox_core.settings import ProdConfig
from ackbox_core.settings import TestConfig

logger = rootlogging.getLogger(__name__)


class Env(object):
    DEV_ENV = 'dev'
    PROD_ENV = 'prod'
    TEST_ENV = 'test'

    def __init__(self):
        self._env = os.environ.get('ACKBOX_ENV_PROFILE', self.PROD_ENV)

    @property
    def is_dev_env(self):
        return self._env == self.DEV_ENV

    @property
    def is_prod_env(self):
        return self._env == self.PROD_ENV

    @property
    def is_test_env(self):
        return self._env == self.TEST_ENV


class Ackbox(object):
    def __init__(self, app, env, config):
        self.app = app
        self.env = env
        self.config = config

    def schedule_cron(self, crons):
        os.system('printf "%s\\n" | crontab -' % '\\n'.join(crons))

    def execute_cmd(self, cmd):
        os.system(cmd)

    def run(self):
        app_host = self.config.ACKBOX_HOST
        app_port = self.config.ACKBOX_PORT
        self.app.run(host=app_host, port=app_port)


class Bootstrap(object):
    def __init__(self, import_name):
        self._app = Flask(import_name, static_url_path='/static', template_folder='/templates')
        self._env = Env()
        self._prod_config = ProdConfig
        self._dev_config = DevConfig
        self._test_config = TestConfig
        self._extensions = None

    @property
    def app(self):
        return self._app

    @property
    def env(self):
        return self._env

    def with_prod_config(self, config):
        self._prod_config = config
        return self

    def with_dev_config(self, config):
        self._dev_config = config
        return self

    def with_test_config(self, config):
        self._test_config = config
        return self

    def with_extensions(self, extensions):
        self._extensions = extensions
        return self

    def build(self):
        config_mappings = self._get_config().get_mappings()
        self._app.config.from_mapping(config_mappings)

        Logger().init_app(self._app)
        Compress().init_app(self._app)
        Assets().init_app(self._app)
        Filters().init_app(self._app)
        CSRFProtect().init_app(self._app)
        Mailer().init_app(self._app)

        if self._extensions:
            self._extensions(self._app)

        with self._app.app_context():
            from ackbox_core import module
            self._app.register_blueprint(module.blueprint)
            self._register_error_handlers()
        return Ackbox(self._app, self._env, self._get_config())

    def _get_config(self):
        if self.env.is_prod_env:
            return self._prod_config
        return self._dev_config if self.env.is_dev_env else self._test_config

    def _register_error_handlers(self):
        """Register error handlers."""

        def render_error(error):
            """Render error template."""
            # If a HTTPException, pull the `code` attribute; default to 500
            logger.error('HTTP error [%s] accessing [%s]' % (error, request.url))
            error_code = getattr(error, 'code', 500)
            return render_template('{0}.html'.format(error_code)), error_code

        for errcode in [401, 404, 500]:
            self._app.errorhandler(errcode)(render_error)
        return None
