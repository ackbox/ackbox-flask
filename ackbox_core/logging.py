import logging


class Logger(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        """Register custom log handlers."""
        log_format = '%(asctime)s - %(levelname)s - %(module)s : %(message)s'
        if not app.debug and app.config.get('ACKBOX_LOG_TO_FILE'):
            from logging.handlers import TimedRotatingFileHandler
            level = logging.INFO
            handler = TimedRotatingFileHandler(app.config.get('ACKBOX_LOG_FILENAME'), when='D', utc=True)
            handler.setLevel(level)
            app.logger.setLevel(level)
            logging.basicConfig(level=level, format=log_format, handlers=[handler])
        else:
            level = logging.DEBUG
            app.logger.setLevel(level)
            logging.basicConfig(level=level, format=log_format)
