from flask_assets import Bundle
from flask_assets import Environment
from htmlmin.main import minify

assets = Environment()


def response_minify(response):
    if response.content_type == u'text/html; charset=utf-8':
        response.set_data(minify(response.get_data(as_text=True)))
        return response
    return response


def create_css_bundle(files):
    return Bundle(*files, filters='cssmin', output='public/css/core-common.css')


def create_core_css_bundle(files):
    return Bundle(*files, filters='cssmin', output='public/css/common.css')


def create_js_bundle(files):
    return Bundle(*files, filters='jsmin', output='public/js/common.js')


def create_core_js_bundle(files):
    return Bundle(*files, filters='jsmin', output='public/js/core-common.js')


class Assets(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        css_assets = app.config.get('ACKBOX_CSS_ASSETS')
        core_css_assets = app.config.get('ACKBOX_CORE_CSS_ASSETS')
        if css_assets:
            assets.register('css_all', create_core_css_bundle(core_css_assets), create_css_bundle(css_assets))
        else:
            assets.register('css_all', create_core_css_bundle(core_css_assets))
        js_assets = app.config.get('ACKBOX_JS_ASSETS')
        core_js_assets = app.config.get('ACKBOX_CORE_JS_ASSETS')
        if js_assets:
            assets.register('js_all', create_core_js_bundle(core_js_assets), create_js_bundle(js_assets))
        else:
            assets.register('js_all', create_core_js_bundle(core_js_assets))
        assets.init_app(app)
        with app.app_context():
            app.after_request(response_minify)
