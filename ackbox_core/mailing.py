import logging
import os

from flask import current_app
from flask import render_template
from flask_mail import Mail
from flask_mail import Message
from premailer import transform

logger = logging.getLogger(__name__)

mail = Mail()


class Mailer(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        mail.init_app(app)


def render_email_template(subject, html_content):
    with open(os.path.join(current_app.config['ACKBOX_STATIC_FOLDER'], 'css/email_style.css')) as css_file:
        css_content = css_file.read()
    return transform(render_template(
        'email_layout.html',
        subject=subject,
        css_content=css_content,
        html_content=html_content))


def send_email(recipient, subject, html_content):
    sender = current_app.config['ACKBOX_MAIL_SENDER']
    logger.info('Sending email to [%s] with subject [%s] as [%s]', recipient, subject, sender)
    message = Message(subject, sender=sender, recipients=[recipient])
    message.html = render_email_template(subject, html_content)
    mail.send(message)
