from datetime import datetime
from datetime import timedelta

from flask import Blueprint
from flask import current_app
from flask import make_response
from flask import render_template
from flask import request
from flask import send_from_directory

RESERVED_NAMESPACES = {'api', 'admin'}

blueprint = Blueprint(
    'ackbox_core',
    __name__,
    static_url_path='/static/ackbox_core',
    static_folder='static',
    template_folder='templates')


def is_reserved_namespace(url):
    return url.split('/')[1] in RESERVED_NAMESPACES


@blueprint.route('/sitemap.xml', methods=['GET'])
def sitemap():
    pages = []
    ten_days_ago = (datetime.now() - timedelta(days=10)).date().isoformat()

    for rule in current_app.url_map.iter_rules():
        if 'GET' in rule.methods and len(rule.arguments) == 0 and not is_reserved_namespace(rule.rule):
            pages.append([rule.rule, ten_days_ago])

    sitemap_xml = render_template('sitemap_template.xml', pages=pages)
    response = make_response(sitemap_xml)
    response.headers['Content-Type'] = 'application/xml'

    return response


@blueprint.route('/robots.txt', methods=['GET'])
def robot():
    if current_app.config['ACKBOX_ALLOW_ROBOTS']:
        return static_from_root('robots_allow.txt')
    return static_from_root('robots_disallow.txt')


@blueprint.route('/health', methods=['GET'])
def health():
    return 'healthy'


@blueprint.route('/.well-known/acme-challenge/<challenge_key>', methods=['GET'])
def acme(challenge_key):
    challenge_response = current_app.config['ACKBOX_ACME_CHALLENGES'][challenge_key]
    return make_response(challenge_response)


def static():
    return static_from_root(request.path[1:])


def static_from_root(filename):
    return send_from_directory(blueprint.static_folder, filename)
