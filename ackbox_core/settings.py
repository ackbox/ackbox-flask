import base64
import datetime
import os

from flask.helpers import get_root_path

from ackbox_core.secrets import get_app_secret
from ackbox_theme.assets import CORE_CSS_ASSETS
from ackbox_theme.assets import CORE_JS_ASSETS


def decode(value):
    return base64.b64decode(value.encode('utf-8')).decode('utf-8')


def env(name, default_value):
    return os.environ.get(name, default_value)


class MetaConfig(object):
    @classmethod
    def get_mappings(cls):
        mappings = {}
        for key in dir(cls):
            if not key.isupper():
                continue
            value = getattr(cls, key)
            if isinstance(value, type) and issubclass(value, MetaConfig):
                mappings.update(value.get_mappings())
            else:
                mappings[key] = value
        mappings.update(cls.get_custom_mappings())
        return mappings

    @classmethod
    def get_custom_mappings(cls):
        return {}

    @classmethod
    def get(cls, name):
        return getattr(cls, name)


class CoreConfig(MetaConfig):
    ACKBOX_HOST = os.environ.get('HOST', 'localhost')
    ACKBOX_PORT = int(os.environ.get('PORT', 8000))
    ACKBOX_SERVER_NAME = 'www.ackbox.com'

    # Website properties
    ACKBOX_HTML_SITE_YEAR = datetime.datetime.now().year
    ACKBOX_HTML_TITLE = 'Ackbox Web'
    ACKBOX_HTML_SITE_NAME = 'Ackbox Web'
    ACKBOX_HTML_SITE_CODE_NAME = 'Web'
    ACKBOX_HTML_DESCRIPTION = 'Ackbox project website'
    ACKBOX_HTML_KEYWORDS = 'Ackbox project website'
    ACKBOX_HTML_URL = ACKBOX_SERVER_NAME
    ACKBOX_HTML_GOOGLE_SITE_VERIFICATION = 'EbAQjZEaNKtkUjzvsiUhReSmKKiQonVWKSiQOND4FUE'

    # Env properties
    ACKBOX_PYTHON_EXEC = '/usr/local/bin/python3'

    # Email properties
    ACKBOX_MAIL_SERVER = 'smtp.gmail.com'
    ACKBOX_MAIL_PORT = 465
    ACKBOX_MAIL_USE_SSL = True
    ACKBOX_MAIL_USE_TSL = False
    ACKBOX_MAIL_USERNAME = 'noreply@ackbox.com'
    ACKBOX_MAIL_PASSWORD = decode('MXEydzNlNHI1dGFja2JveG5vcmVwbHk=')
    ACKBOX_MAIL_SENDER = ('Ackbox', ACKBOX_MAIL_USERNAME)

    # Theme properties
    ACKBOX_STATIC_FOLDER = os.path.join(get_root_path('ackbox_theme'), 'static')
    ACKBOX_CORE_CSS_ASSETS = CORE_CSS_ASSETS
    ACKBOX_CORE_JS_ASSETS = CORE_JS_ASSETS
    ACKBOX_CSS_ASSETS = ()
    ACKBOX_JS_ASSETS = ()

    # General properties
    ACKBOX_APP_SECRET_KEY = env('ACKBOX_APP_SECRET_KEY', get_app_secret())
    ACKBOX_ALLOW_ROBOTS = True
    ACKBOX_ACME_CHALLENGES = {}
    ACKBOX_PROFILE = env('ACKBOX_PROFILE', 'dev')
    ACKBOX_USE_SSL = env('ACKBOX_USE_SSL', False)
    ACKBOX_DEBUG = ACKBOX_PROFILE == 'dev'
    ACKBOX_LOG_TO_FILE = env('ACKBOX_LOG_TO_FILE', True)
    ACKBOX_LOG_FILENAME = '/var/log/ackbox.log'
    ACKBOX_ASSETS_DEBUG = env('ACKBOX_ASSETS_DEBUG', False)
    ACKBOX_ASSETS_USE_S3 = False

    @classmethod
    def get_custom_mappings(cls):
        return {
            # Overrides for Flask
            'SERVER_NAME': cls.ACKBOX_SERVER_NAME if not cls.ACKBOX_DEBUG else '%s:%s' % (cls.ACKBOX_HOST, cls.ACKBOX_PORT),
            'SECRET_KEY': cls.ACKBOX_APP_SECRET_KEY,
            'DEBUG': cls.ACKBOX_DEBUG,
            # Overrides for FlaskAssets
            'ASSETS_DEBUG': cls.ACKBOX_ASSETS_DEBUG,
            'FLASK_ASSETS_USE_S3': cls.ACKBOX_ASSETS_USE_S3,
            # Overrides for FlaskMail
            'MAIL_SERVER': cls.ACKBOX_MAIL_SERVER,
            'MAIL_PORT': cls.ACKBOX_MAIL_PORT,
            'MAIL_USE_SSL': cls.ACKBOX_MAIL_USE_SSL,
            'MAIL_USE_TSL': cls.ACKBOX_MAIL_USE_TSL,
            'MAIL_USERNAME': cls.ACKBOX_MAIL_USERNAME,
            'MAIL_PASSWORD': cls.ACKBOX_MAIL_PASSWORD
        }


class ProdConfig(CoreConfig):
    # App properties
    ACKBOX_DEBUG = False


class DevConfig(CoreConfig):
    # App properties
    ACKBOX_DEBUG = CoreConfig.ACKBOX_DEBUG


class TestConfig(CoreConfig):
    # App properties
    ACKBOX_DEBUG = CoreConfig.ACKBOX_DEBUG
