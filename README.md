# Ackbox Flask

Helpers allowing fast prototyping of websites.

## Installation:
Reference `ackbox-flask` library directly in your `requirements.txt`:

    -e git+https://git@bitbucket.org/ackbox/ackbox-flask.git#egg=ackbox-flask

Or directly installing by:

    pip3 install git+https://git@bitbucket.org/ackbox/ackbox-flask.git#egg=ackbox-flask

## Bootstrap:
Sample usage with `Core`, `Persistence`, `Resources` and `Security` modules.

```python
from ackbox_core import Bootstrap
from ackbox_persistence import Persistence
from ackbox_resources import Resources
from ackbox_security import Security

from yourapp.settings import DevConfig
from yourapp.settings import ProdConfig


persistence = Persistence()
resources = Resources()
security = Security()


def register_extensions(ackbox_app):
    """Register Flask extensions."""
    persistence.init_app(ackbox_app)
    resources.init_app(ackbox_app, url_prefix=None)
    security.init_app(ackbox_app, url_prefix=None)
    return None


ackbox = Bootstrap(__name__) \
    .with_prod_config(ProdConfig) \
    .with_dev_config(DevConfig) \
    .with_extensions(register_extensions) \
    .build()

app = ackbox.app
is_dev_env = ackbox.is_dev_env

```

## Running:
Update lib and run your application in development environment by executing:

    pip3 uninstall ackbox-flask -y; pip3 install -r dev_requirements.txt; export ACKBOX_ENV_PROFILE=dev; python run.py

If you're running the application in production environment, simply execute:

    pip3 install -r requirements.txt; python run.py

## Tasks
- Improve redirection after register and when accessing restricted pages.
- Restrict login retries after a certain amount of time.
