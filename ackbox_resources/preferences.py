from flask_mongoengine.wtf import model_form
from flask_mongoengine.wtf.orm import model_fields

from ackbox_resources.forms import PatchedModelForm
from ackbox_resources.exceptions import ModelPermissionException
from ackbox_security.models import ROLE_USER
from ackbox_security.validation import is_user_authenticated
from ackbox_security.validation import is_user_authorized


class Pagination(object):
    DEFAULT_ITEMS_PER_PAGE = 100

    def __init__(self, enabled=False, items_per_page=None):
        self.enabled = enabled
        self.items_per_page = items_per_page or self.DEFAULT_ITEMS_PER_PAGE


class SortedBy(object):
    def __init__(self, enabled=False, field_name=None):
        self.enabled = enabled
        self.field_name = field_name


class FieldFormatter(object):
    def __init__(self, enabled=False, formatters=None):
        self.enabled = enabled
        self.formatters = formatters or {}

    def format(self, field_name, field_value):
        def noop_formatter(value):
            return value

        formatter = self.formatters.get(field_name, noop_formatter)
        return formatter(field_value)


class Permissions(object):
    DEFAULT_PERMISSIONS = {
        'index': [ROLE_USER],
        'show': [ROLE_USER],
        'new': [ROLE_USER],
        'create': [ROLE_USER],
        'edit': [ROLE_USER],
        'update': [ROLE_USER],
        'destroy': [ROLE_USER]
    }

    def __init__(self, permissions=None):
        self._permissions = permissions or self.DEFAULT_PERMISSIONS

    def verify_permissions(self, action):
        if action not in self._permissions:
            raise ModelPermissionException('Action [%s] not listed in permissions' % action)
        required_roles = self._permissions[action]
        if required_roles:
            if not is_user_authenticated():
                raise ModelPermissionException('Authentication required to access action [%s]' % action)
            if not is_user_authorized(required_roles):
                raise ModelPermissionException('Authorization required to access action [%s]' % action)

    def has_permissions(self, action):
        try:
            self.verify_permissions(action)
            return True
        except ModelPermissionException:
            return False

    def to_dict(self):
        return {action: self.has_permissions(action) for action in self._permissions.keys()}


class PresentationPreferences(object):
    def __init__(self, excluded_fields=None, field_args=None):
        self._excluded_fields = excluded_fields
        self._field_args = field_args
        self._field_formatter = FieldFormatter()
        self._pagination = Pagination()
        self._sorted_by = SortedBy()
        self._permissions = Permissions()

    def with_excluded_fields(self, excluded_fields):
        self._excluded_fields = excluded_fields
        return self

    def with_field_args(self, field_args):
        self._field_args = field_args
        return self

    def with_field_formatters(self, field_formatters):
        self._field_formatter = FieldFormatter(enabled=True, formatters=field_formatters)
        return self

    def with_pagination(self, items_per_page):
        self._pagination = Pagination(enabled=True, items_per_page=items_per_page)
        return self

    def with_sorted_by(self, field_name):
        self._sorted_by = SortedBy(enabled=True, field_name=field_name)
        return self

    def with_permissions(self, permissions):
        self._permissions = Permissions(permissions)
        return self

    @property
    def pagination(self):
        return self._pagination

    @property
    def field_formatter(self):
        return self._field_formatter

    @property
    def sorted_by(self):
        return self._sorted_by

    @property
    def permissions(self):
        return self._permissions

    def get_field_names(self, model_class):
        return [key for key in model_fields(model_class, exclude=self._excluded_fields).keys()]

    def get_form_class(self, model_class):
        return model_form(model_class, base_class=PatchedModelForm, exclude=self._excluded_fields, field_args=self._field_args)
