class Resources(object):
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app, url_prefix=None):
        with app.app_context():
            from ackbox_resources import module
            for endpoint in app.config['ACKBOX_RESOURCES_FACTORY']():
                module.register_endpoint_resource(endpoint)
            app.register_blueprint(module.blueprint, url_prefix=url_prefix)
