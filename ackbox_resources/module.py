import logging

from flask import Blueprint

logger = logging.getLogger(__name__)

model_presentations = {}

blueprint = Blueprint('ackbox_resources', __name__, static_url_path='/static/ackbox_resources')


def register_endpoint_resource(endpoint):
    actions = endpoint.actions
    options = {'strict_slashes': False}
    logger.info('Registering endpoint [%s] -> [%s]' % (actions.model_prefix, actions.model_path))

    blueprint.add_url_rule(actions.index_path, endpoint=actions.index_endpoint, view_func=endpoint.index_action, methods=['GET'], **options)
    blueprint.add_url_rule(actions.show_path, endpoint=actions.show_endpoint, view_func=endpoint.show_action, methods=['GET'], **options)
    blueprint.add_url_rule(actions.new_path, endpoint=actions.new_endpoint, view_func=endpoint.new_action, methods=['GET'], **options)
    blueprint.add_url_rule(actions.create_path, endpoint=actions.create_endpoint, view_func=endpoint.create_action, methods=['POST'], **options)
    blueprint.add_url_rule(actions.edit_path, endpoint=actions.edit_endpoint, view_func=endpoint.edit_action, methods=['GET'], **options)
    blueprint.add_url_rule(actions.update_path, endpoint=actions.update_endpoint, view_func=endpoint.update_action, methods=['POST'], **options)
    blueprint.add_url_rule(actions.destroy_path, endpoint=actions.destroy_endpoint, view_func=endpoint.destroy_action, methods=['DELETE'], **options)
