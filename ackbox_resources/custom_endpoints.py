import calendar
import logging
import pytz
from datetime import datetime

from flask import render_template
from flask import request
from mongoengine import Q

from ackbox_resources.endpoints import HtmlResourceEndpoint

logger = logging.getLogger(__name__)


class TextFilter(object):
    def __init__(self, model_presentation, query_args):
        self._term = query_args.get('text')

    def query(self, cursor):
        return cursor.search_text(self._term) if self._term else cursor

    def args(self):
        return {
            'feature_text_filter_enabled': True,
            'feature_text_filter_term': self._term
        }


class TimeFilter(object):
    def __init__(self, model_presentation, query_args, datetime_field='created_at'):
        self._datetime_field = datetime_field
        self._term = query_args.get('time')
        self._time_filter_options = self._retrieve_time_filter_options(model_presentation.model_class)

    def query(self, cursor):
        time_filter_term = self._term or self._time_filter_options[0]
        year, month = time_filter_term.split('-')
        from_date = datetime(
            int(year),
            month=int(month),
            day=1,
            hour=0,
            minute=0,
            second=0,
            microsecond=0,
            tzinfo=pytz.utc
        )
        to_date = datetime(
            int(year),
            month=int(month),
            day=calendar.monthrange(int(year), int(month))[1],
            hour=23,
            minute=59,
            second=59,
            microsecond=999999,
            tzinfo=pytz.utc
        )
        return cursor.filter(Q(created_at__gte=from_date) & Q(created_at__lte=to_date))

    def args(self):
        return {
            'feature_time_filter_enabled': True,
            'feature_time_filter_term': self._term,
            'feature_time_filter_options': self._time_filter_options
        }

    def _retrieve_time_filter_options(self, model_class):
        pipeline = [{'$group': {'_id': {'$substr': ['$%s' % self._datetime_field, 0, 7]}, 'sum': {'$sum': 1}}}]
        options = sorted([entry['_id'] for entry in model_class.objects.aggregate(*pipeline)], reverse=True)
        if not options:
            now = datetime.now()
            options = ['%d-%02d' % (now.year, now.month)]
        return options


class TotalAmountProcessor(object):
    def __init__(self, model_presentation, query_args, amount_field='amount'):
        self._model_presentation = model_presentation
        self._amount_field = amount_field

    def args(self, listing):
        return {
            'feature_total_amount_enabled': True,
            'feature_total_amount_value': sum([item.get_instance()[self._amount_field] for item in listing.get_model_instances()]),
            'feature_total_amount_fields': len(listing.model_fields),
        }


class ModularHtmlResourceEndpoint(HtmlResourceEndpoint):
    FILTERS = []
    PROCESSORS = []

    def index(self, **kwargs):
        title = 'Listing %s' % self.model_presentation.model_title
        filter_queries, filter_args = self._get_filter_context(self.model_presentation, request.args)
        listing = self.model_presentation.list(request.args, query_filters=filter_queries)
        processor_args = self._get_processor_context(request.args, listing)
        return render_template(
            'resources/list_resources.html',
            title=title,
            feature_pagination_enabled=False,
            feature_pagination=self._build_pagination(listing),
            model_permissions=self.model_presentation.model_permissions,
            actions=self.actions,
            **filter_args,
            **processor_args,
            **listing.raw())

    def _get_filter_context(self, model_presentation, query_args):
        filter_queries = []
        filter_args = {}
        for filter_class in self.FILTERS:
            filter_instance = filter_class(model_presentation, query_args)
            filter_queries.append(filter_instance.query)
            filter_args.update(filter_instance.args())

        return filter_queries, filter_args

    def _get_processor_context(self, query_args, listing):
        processor_args = {}
        for processor_class in self.PROCESSORS:
            processor_instance = processor_class(self.model_presentation, query_args)
            processor_args.update(processor_instance.args(listing))

        return processor_args


class TabledHtmlResourceEndpoint(ModularHtmlResourceEndpoint):
    FILTERS = [TimeFilter, TextFilter]
    PROCESSORS = [TotalAmountProcessor]
