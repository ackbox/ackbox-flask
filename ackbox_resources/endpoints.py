import logging
from functools import wraps

from flask import abort
from flask import flash
from flask import jsonify
from flask import render_template
from flask import request
from flask import session
from flask_paginate import Pagination

from ackbox_resources.exceptions import ModelPermissionException
from ackbox_resources.exceptions import ModelPresentationException
from ackbox_resources.exceptions import ModelValidationException
from ackbox_resources.exceptions import ModelException
from ackbox_resources.models import ModelPresentation

logger = logging.getLogger(__name__)


class JsonFormData(dict):
    def getlist(self, key):
        return self[key]

    @classmethod
    def from_request_form(cls, form):
        raw = {}
        session_data = {'user_id': session['user_id']}
        for each in (form, session_data):
            raw.update({key: [value] for key, value in each.items()})
        return JsonFormData(raw)

    def __repr__(self):
        return type(self).__name__ + '(%s)' % dict.__repr__(self)


def requires_action_permission(func):
    @wraps(func)
    def decorated_view(self, *args, **kwargs):
        func_name = func.__name__.split('_')[0]
        if func_name not in self.ALLOWED_ACTIONS:
            abort(403)
        return func(self, *args, **kwargs)

    return decorated_view


class EndpointActions(object):
    def __init__(self, model_key, url_prefix):
        self.model_prefix = self._get_prefix(model_key, url_prefix)
        self.model_path = self._get_model_path(model_key, url_prefix)
        self.mode_path_new = '%s/new' % self.model_path
        self.model_path_id = '%s/<model_id>' % self.model_path
        self.model_path_id_edit = '%s/<model_id>/edit' % self.model_path

    @property
    def index_path(self):
        return self.model_path

    @property
    def show_path(self):
        return self.model_path_id

    @property
    def new_path(self):
        return self.mode_path_new

    @property
    def create_path(self):
        return self.model_path

    @property
    def edit_path(self):
        return self.model_path_id_edit

    @property
    def update_path(self):
        return self.model_path_id

    @property
    def destroy_path(self):
        return self.model_path_id

    @property
    def index_endpoint(self):
        return '%s_index' % self.model_prefix

    @property
    def show_endpoint(self):
        return '%s_show' % self.model_prefix

    @property
    def new_endpoint(self):
        return '%s_new' % self.model_prefix

    @property
    def create_endpoint(self):
        return '%s_create' % self.model_prefix

    @property
    def edit_endpoint(self):
        return '%s_edit' % self.model_prefix

    @property
    def update_endpoint(self):
        return '%s_update' % self.model_prefix

    @property
    def destroy_endpoint(self):
        return '%s_destroy' % self.model_prefix

    @staticmethod
    def _get_prefix(model_key, url_prefix):
        return '_'.join((model_key, *url_prefix.split('/')[1:])) if url_prefix else model_key

    @staticmethod
    def _get_model_path(model_key, url_prefix):
        return '%s/%s' % (url_prefix, model_key)


class ResourceEndpoint(object):
    def __init__(self, model_presentation, url_prefix):
        self.model_presentation = model_presentation
        self.url_prefix = url_prefix
        self.actions = EndpointActions(model_presentation.model_key, url_prefix)

    @property
    def model_key(self):
        return self.model_presentation.model_key

    def __getattribute__(self, name):
        if name.endswith('_action'):
            action_name = name.split('_action')[0]
            action = object.__getattribute__(self, action_name)

            def decorator(*args, **kwargs):
                logger.info('Received request for action [%s:%s]' % (self.model_presentation.model_key, action_name))
                self.verify_permissions(action_name, self.model_presentation)
                return action(*args, **kwargs)

            return decorator

        return object.__getattribute__(self, name)

    @staticmethod
    def verify_permissions(action_name, model_presentation):
        model_key = model_presentation.model_key
        logger.info('Checking authorization permissions for [%s:%s]' % (model_key, action_name))
        try:
            model_presentation.prefs.permissions.verify_permissions(action_name)
        except ModelPermissionException as e:
            logger.warning('Action [%s:%s] not allowed -> %s' % (model_key, action_name, e))
            abort(401)

    @classmethod
    def from_model_class(cls, model_class, prefs, url_prefix=''):
        model_presentation = ModelPresentation(model_class, prefs)
        return cls(model_presentation, url_prefix=url_prefix)

    @classmethod
    def from_model_presentation(cls, model_presentation, url_prefix=''):
        return cls(model_presentation, url_prefix=url_prefix)


class HtmlResourceEndpoint(ResourceEndpoint):
    def index(self, **kwargs):
        title = 'Listing %s' % self.model_presentation.model_title
        listing = self.model_presentation.list(request.args, query_filters=None)
        return render_template(
            'resources/list_resources.html',
            title=title,
            featrure_pagination_enabled=self.model_presentation.prefs.pagination.enabled,
            featrure_pagination=self._build_pagination(listing),
            model_permissions=self.model_presentation.model_permissions,
            actions=self.actions,
            **listing.raw())

    def show(self, model_id, **kwargs):
        title = 'Showing %s' % self.model_presentation.model_title
        return render_template(
            'resources/show_resource.html',
            title=title,
            model_permissions=self.model_presentation.model_permissions,
            actions=self.actions,
            **self.model_presentation.get(model_id).raw())

    def new(self, **kwargs):
        form = kwargs.get('form') or self.model_presentation.get_form(request.form)
        title = 'Creating %s' % self.model_presentation.model_title
        params = {'model_key': self.model_presentation.model_key, 'form': form}
        return render_template(
            'resources/new_resource.html',
            title=title,
            http_method='POST',
            model_permissions=self.model_presentation.model_permissions,
            actions=self.actions,
            **params)

    def create(self, **kwargs):
        try:
            model_id = self.model_presentation.create_or_edit(request.form)
            logger.info('Model %s:%s successfully created' % (self.model_presentation.model_key, model_id))
            flash('%s record successfully created' % self.model_presentation.model_title)
            return self.show(model_id)
        except ModelValidationException as e:
            logger.warning('Model %s had validation issues' % self.model_presentation.model_key)
            flash('%s record could not be created' % self.model_presentation.model_title, category='warning')
            return self.new(form=e.form, **kwargs)
        except ModelPresentationException as e:
            logger.warning('%s record could not be created: %s' % (self.model_presentation.model_key, e))
            flash('%s record could not be created' % self.model_presentation.model_title, category='warning')
            return self.index(**kwargs)

    def edit(self, model_id, **kwargs):
        form = kwargs.get('form') or self.model_presentation.get_form(request.form, model_id)
        title = 'Editing %s' % self.model_presentation.model_title
        params = {'model_key': self.model_presentation.model_key, 'model_id': model_id, 'form': form}
        return render_template(
            'resources/edit_resource.html',
            title=title,
            http_method='POST',
            model_permissions=self.model_presentation.model_permissions,
            actions=self.actions,
            **params)

    def update(self, model_id, **kwargs):
        try:
            model_id = self.model_presentation.create_or_edit(request.form, model_id=model_id)
            logger.info('Model %s:%s successfully updated' % (self.model_presentation.model_key, model_id))
            flash('%s record successfully updated' % self.model_presentation.model_title)
            return self.show(model_id)
        except ModelValidationException as e:
            logger.warning('Model %s:%s had validation issues' % (self.model_presentation.model_key, model_id))
            flash('%s record could not be updated' % self.model_presentation.model_title, category='warning')
            return self.edit(model_id, form=e.form, **kwargs)
        except ModelPresentationException as e:
            logger.warning('%s record could be updated: %s' % (self.model_presentation.model_key, e))
            flash('%s record could not be updated' % self.model_presentation.model_title, category='warning')
            return self.index(**kwargs)

    def destroy(self, model_id, **kwargs):
        try:
            self.model_presentation.delete(model_id)
            logger.info('Model %s:%s successfully deleted' % (self.model_presentation.model_key, model_id))
            flash('%s successfully deleted' % self.model_presentation.model_title)
        except ModelPresentationException as e:
            logger.warning('Model %s:%s could not be deleted: %s' % (self.model_presentation.model_key, model_id, e))
            flash('%s record could not be deleted: %s' % (self.model_presentation.model_title, e), category='warning')
        return self.index(**kwargs)

    def _build_pagination(self, listing):
        return Pagination(
            page=listing.model_items_page,
            per_page=self.model_presentation.prefs.pagination.items_per_page,
            found=listing.model_items_size,
            total=listing.model_items_size,
            search=False,
            record_name=self.model_presentation.model_title,
            bs_version=3,
            link_size='sm')


class RestJsonResourceEndpoint(ResourceEndpoint):
    def index(self, **kwargs):
        try:
            return self._data_response(self.model_presentation.list(request.args).raw()['model_items'], 200)
        except ModelPresentationException as e:
            logger.warning('%s records could be retrieved: %s' % (self.model_presentation.model_key, e))
            return self._message_response('%s records could not be retrieved' % self.model_presentation.model_title, 400)

    def show(self, model_id, **kwargs):
        try:
            return self._data_response(self.model_presentation.get(model_id).raw()['model_item'], 200)
        except ModelException as e:
            logger.warning('%s record [%s] could be retrieved: %s' % (self.model_presentation.model_key, model_id,  e))
            return self._message_response('%s record could not be retrieved' % self.model_presentation.model_title, 400)

    def new(self, **kwargs):
        msg = 'Successfully submitted %s:%s record creation request' % self.model_presentation.model_title
        return self._data_response(msg, 200)

    def create(self, **kwargs):
        try:
            form = JsonFormData.from_request_form(request.form)
            model_id = self.model_presentation.create_or_edit(form)
            logger.info('Model %s:%s successfully created' % (self.model_presentation.model_key, model_id))
            return self._message_response('%s record successfully created' % self.model_presentation.model_title, 200)
        except ModelException as e:
            logger.warning('%s record could be updated: %s' % (self.model_presentation.model_key, e))
            return self._message_response('%s record could not be created' % self.model_presentation.model_title, 400)

    def edit(self, model_id, **kwargs):
        msg = 'Successfully submitted %s:%s record edition request' % (self.model_presentation.model_title, model_id)
        return self._message_response(msg, 200)

    def update(self, model_id, **kwargs):
        try:
            form = JsonFormData.from_request_form(request.form)
            model_id = self.model_presentation.create_or_edit(form, model_id=model_id)
            logger.info('Model %s:%s successfully updated' % (self.model_presentation.model_key, model_id))
            return self._message_response('%s record successfully updated' % self.model_presentation.model_title, 200)
        except ModelException as e:
            logger.warning('%s record could be updated: %s' % (self.model_presentation.model_key, e))
            return self._message_response('%s record could not be updated' % self.model_presentation.model_title, 400)

    def destroy(self, model_id, **kwargs):
        try:
            self.model_presentation.delete(model_id)
            logger.info('Model %s:%s successfully deleted' % (self.model_presentation.model_key, model_id))
            return self._message_response('%s record successfully deleted' % self.model_presentation.model_title, 200)
        except ModelException as e:
            logger.warning('Model %s:%s could not be deleted: %s' % (self.model_presentation.model_key, model_id, e))
            return self._message_response('%s record could not be deleted' % self.model_presentation.model_title, 404)

    @staticmethod
    def _message_response(message, code):
        return jsonify({'message': message}), code

    @staticmethod
    def _data_response(data, code):
        return jsonify(data), code


def model_id_url(model_key, model_id):
    return '/%s/%s' % (model_key, model_id)
