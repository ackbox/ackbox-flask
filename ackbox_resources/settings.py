from ackbox_core.settings import MetaConfig


def noop():
    pass


class ResourcesCoreConfig(MetaConfig):
    ACKBOX_RESOURCES_FACTORY = noop
