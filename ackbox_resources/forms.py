from flask_mongoengine.wtf.orm import model_fields
from flask_mongoengine.wtf.orm import ModelForm


class PatchedModelForm(ModelForm):

    def save(self, commit=True, **kwargs):
        if self.instance:
            self.populate_obj(self.instance)
        else:
            valid_data = {field: self.data.get(field) for field in model_fields(self.model_class)}
            self.instance = self.model_class(**valid_data)

        if commit:
            self.instance.save(**kwargs)
        return self.instance