import logging
import re
from collections import OrderedDict

from ackbox_resources.exceptions import ModelPresentationException
from ackbox_resources.exceptions import ModelValidationException
from ackbox_resources.preferences import PresentationPreferences
from ackbox_security import current_user

logger = logging.getLogger(__name__)


class MetaModel(object):
    def __init__(self, model_class):
        self._model_class = model_class

    def raw(self):
        accessible_methods = [name for name, value in vars(self.__class__).items() if isinstance(value, property)]
        return {accessible_method: getattr(self, accessible_method) for accessible_method in accessible_methods}


class Model(MetaModel):
    def __init__(self, model_class, prefs, model_instance):
        super().__init__(model_class)
        self._model_instance = model_instance
        self._prefs = prefs

    @property
    def model_key(self):
        return generate_model_key(self._model_class)

    @property
    def model_id(self):
        return self._model_instance.id

    @property
    def model_fields(self):
        return self._prefs.get_field_names(self._model_class)

    @property
    def model_item(self):
        return self._convert_to_dict(self._model_instance)

    def get_instance(self):
        return self._model_instance

    def _convert_to_dict(self, item):
        model_dict = OrderedDict([('id', str(item.id))])
        for model_field_name in self.model_fields:
            value = getattr(item, model_field_name)
            model_dict[model_field_name] = self._prefs.field_formatter.format(model_field_name, value)
        return model_dict

    @classmethod
    def from_id(cls, model_class, prefs, model_id):
        return Model(model_class, prefs, model_class.objects.get(id=model_id))

    @classmethod
    def from_instance(cls, model_class, prefs, instance):
        return Model(model_class, prefs, instance)


class ListModel(MetaModel):
    def __init__(self, model_class, prefs, query_args, query_filters=None):
        super().__init__(model_class)
        self._prefs = prefs
        self._query_args = query_args
        self._query_filters = query_filters

    def _get_cursor(self):
        cursor = self._model_class.objects
        if self._prefs.sorted_by.enabled:
            cursor = cursor.order_by(self._prefs.sorted_by.field_name)

        if self._prefs.pagination.enabled:
            items_per_page = self._prefs.pagination.items_per_page
            offset = (self.model_items_page - 1) * items_per_page
            cursor = cursor.skip(offset).limit(items_per_page)

        if self._query_filters:
            for query in self._query_filters:
                cursor = query(cursor)

        return cursor

    def get_model_instances(self):
        return [Model.from_instance(self._model_class, self._prefs, inst) for inst in self._get_cursor()]

    @property
    def model_key(self):
        return generate_model_key(self._model_class)

    @property
    def model_fields(self):
        return self._prefs.get_field_names(self._model_class)

    @property
    def model_items(self):
        return [model_instance.model_item for model_instance in self.get_model_instances()]

    @property
    def model_items_size(self):
        return self._model_class.objects.count()

    @property
    def model_items_page(self):
        return int(self._query_args.get('page', 1))


class ModelPresentation(object):
    def __init__(self, model_class, prefs=PresentationPreferences()):
        self._model_class = model_class
        self._prefs = prefs

    @property
    def prefs(self):
        return self._prefs

    @property
    def model_key(self):
        return generate_model_key(self._model_class)

    @property
    def model_class(self):
        return self._model_class

    @property
    def model_permissions(self):
        return self._prefs.permissions.to_dict()

    @property
    def model_title(self):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1 \2', str(self.model_class.__name__))
        return re.sub('([a-z0-9])([A-Z])', r'\1 \2', s1).title()

    def list(self, query_args, query_filters=None):
        return ListModel(self._model_class, self._prefs, query_args, query_filters=query_filters)

    def get(self, model_id):
        return Model.from_id(self._model_class, self._prefs, model_id)

    def get_instance(self, model_id):
        return self.get(model_id).get_instance()

    def create_or_edit(self, model_params, model_id=None):
        form = self.get_form(model_params, model_id=model_id)
        if not form.validate():
            raise ModelValidationException(self.model_key, form)
        try:
            instance = form.save(commit=False)
            instance.save()
            return instance.id
        except Exception as e:
            raise ModelPresentationException('Error creating or editing model [%s]' % self.model_key, e)

    def delete(self, model_id):
        try:
            self.get_instance(model_id).delete()
        except Exception as e:
            raise ModelPresentationException('Error deleting model [%s]' % self.model_key, e)

    def get_form(self, model_params, model_id=None):
        instance = self.get_instance(model_id) if model_id else None
        return self._prefs.get_form_class(self._model_class)(model_params, obj=instance)


class UserModelPresentation(ModelPresentation):
    def create_or_edit(self, model_params, model_id=None):
        form = self.get_form(model_params, model_id=model_id)
        if not form.validate():
            raise ModelValidationException(self.model_key, form)
        try:
            new_model_instance = form.save(commit=False)
            if model_id:
                old_model_instance = self.get_instance(model_id)
                if old_model_instance.password != new_model_instance.password:
                    plaintext_password = new_model_instance.password
                    new_model_instance.set_password(plaintext_password)
            else:
                plaintext_password = new_model_instance.password
                new_model_instance.set_password(plaintext_password)
            new_model_instance.save()
            return new_model_instance.id
        except Exception as e:
            raise ModelPresentationException('Error creating or editing model [%s]' % self.model_key, e)


class OwnedModelPresentation(ModelPresentation):
    def list(self, query_args, query_filters=None):
        def create_ownership_filter(cursor):
            return cursor(user_id=current_user.get_id())

        filters = query_filters[:] if query_filters else []
        filters.extend([create_ownership_filter])
        return ListModel(self._model_class, self._prefs, query_args, query_filters=filters)

    def get(self, model_id):
        model_presentation = Model.from_id(self._model_class, self._prefs, model_id)
        self._validate_ownership(model_presentation.get_instance())
        return model_presentation

    def create_or_edit(self, model_params, model_id=None):
        self._validate_ownership(self.get_instance(model_id) if model_id else None)
        form = self.get_form(model_params, model_id=model_id)
        if not form.validate():
            raise ModelValidationException(self.model_key, form)
        try:
            instance = form.save(commit=False)
            instance.save()
            return instance.id
        except Exception as e:
            raise ModelPresentationException('Error creating or editing model [%s]' % self.model_key, e)

    def delete(self, model_id):
        try:
            instance = self.get_instance(model_id)
            self._validate_ownership(instance)
            instance.delete()
        except Exception as e:
            raise ModelPresentationException('Error deleting model [%s]' % self.model_key, e)

    @staticmethod
    def _validate_ownership(model_instance):
        if model_instance and not str(model_instance.user_id) == str(current_user.get_id()):
            raise ModelPresentationException('User [%s] tried to perform an operation not permitted' % current_user.get_id())


def generate_model_key(model_class):
    return str(model_class.__name__).lower()
