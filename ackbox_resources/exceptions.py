class ModelException(Exception):
    pass


class ModelPresentationException(ModelException):
    def __init__(self, message=None, cause=None):
        self.cause = cause
        self.message = message

    def __str__(self):
        return '%s -> caused by %s' % (self.message, self.cause)


class ModelPermissionException(ModelException):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return 'Permission exception -> %s' % self.message


class ModelValidationException(ModelException):
    def __init__(self, model_key, form):
        self.model_key = model_key
        self.form = form

    def __str__(self):
        return 'Model %s validation errors -> %s' % (self.model_key, self.form.errors)
