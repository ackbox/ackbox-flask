import typing

import stripe

from ackbox_core.common import boundary
from ackbox_payments.exceptions import CardValidationException
from ackbox_payments.exceptions import PaymentServiceException
from ackbox_payments.gateway import PaymentGateway
from ackbox_payments.models import Card
from ackbox_payments.models import CardValidation
from ackbox_payments.models import Customer
from ackbox_payments.models import Plan
from ackbox_payments.models import Subscription
from ackbox_payments.models import Token


class StripePaymentService(PaymentGateway):
    def __init__(self, config):
        stripe.api_key = config['STRIPE_API_KEY']

    @boundary(CardValidationException)
    def validate_card(self, card: Card) -> CardValidation:
        stripe_token = stripe.Token.create(
            card={
                'number': card.number,
                'exp_month': card.exp_month,
                'exp_year': card.exp_year,
                'cvc': card.cvc
            }
        )
        return CardValidation(card, Token(token_id=stripe_token.id))

    @boundary(PaymentServiceException)
    def create_customer(self, user_id: str, source: str) -> Customer:
        stripe_customer = stripe.Customer.create(metadata={'user_id': user_id})
        return Customer(user_id=user_id, customer_id=stripe_customer.id)

    @boundary(PaymentServiceException)
    def create_plan(self, plan: Plan) -> Plan:
        stripe_plan = stripe.Plan.create(
            name=plan.name,
            id=plan.plan_id,
            interval=plan.interval,
            currency=plan.currency,
            amount=plan.amount,
        )
        return convert_plan(stripe_plan)

    @boundary(PaymentServiceException)
    def retrieve_plans(self) -> typing.List[Plan]:
        stripe_plans = stripe.Plan.list()
        return [convert_plan(stripe_plan) for stripe_plan in stripe_plans]

    @boundary(PaymentServiceException)
    def create_subscription(self, customer_id: str, plan_id: str) -> Subscription:
        stripe_subscription = stripe.Subscription.create(
            customer=customer_id,
            plan=plan_id,
        )
        return convert_subscription(stripe_subscription)

    @boundary(PaymentServiceException)
    def retrieve_subscriptions(self, customer_id: str) -> typing.List[Subscription]:
        stripe_subscriptions = stripe.Subscription.list(customer=customer_id)
        return [convert_subscription(stripe_subscription) for stripe_subscription in stripe_subscriptions]

    @boundary(PaymentServiceException)
    def cancel_subscription(self, subscription_id: str):
        stripe.Subscription.retrieve(subscription_id).delete()


def convert_subscription(stripe_subscription):
    return Subscription(
        customer_id=stripe_subscription.customer,
        plan_id=stripe_subscription.plan.id,
        subscription_id=stripe_subscription.id,
        status=stripe_subscription.status
    )


def convert_plan(stripe_plan):
    return Plan(
        plan_id=stripe_plan.id,
        name=stripe_plan.name,
        interval=stripe_plan.interval,
        currency=stripe_plan.currency,
        amount=stripe_plan.amount,
        trial_period_days=stripe_plan.trial_period_days,
    )
