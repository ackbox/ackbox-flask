from ackbox_payments.models import Customer


class PaymentDatastore(object):
    def create_customer(self, customer: Customer):
        raise NotImplemented()

    def retrieve_customer(self, user_id: str) -> Customer:
        raise NotImplemented()
