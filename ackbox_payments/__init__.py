from flask import current_app
from werkzeug.local import LocalProxy

from ackbox_payments.service import PaymentService


def build_gateway():
    gateway_config = current_app.config['ACKBOX_PAYMENT_GATEWAY_CONFIG']
    return current_app.config['ACKBOX_PAYMENT_GATEWAY_CLASS'](gateway_config)


def build_datastore():
    return current_app.config['ACKBOX_PAYMENT_DATASTORE_CLASS']()


gateway = LocalProxy(lambda: build_gateway)
datastore = LocalProxy(lambda: build_datastore)
service = PaymentService(gateway, datastore)


class Payments(object):
    def __init__(self, app=None, url_prefix=None, register_view=True):
        if app is not None:
            self.init_app(app, url_prefix=url_prefix, register_view=register_view)

    def init_app(self, app, url_prefix=None, register_view=True):
        with app.app_context():
            service.ensure_plans(current_app.config['ACKBOX_PAYMENT_PLANS'])

            from ackbox_payments import module
            if register_view:
                app.register_blueprint(module.blueprint, url_prefix=url_prefix)
