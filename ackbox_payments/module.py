import logging

from flask import Blueprint
from flask import flash
from flask import redirect
from flask import render_template
from flask import request

from ackbox_payments import service
from ackbox_payments.exceptions import CardValidationException
from ackbox_payments.exceptions import PaymentServiceException
from ackbox_payments.forms import SubscriptionForm
from ackbox_payments.models import Card
from ackbox_security import current_user

logger = logging.getLogger(__name__)

blueprint = Blueprint('ackbox_payments', __name__, static_url_path='/static/ackbox_payments')


@blueprint.route('/subscription/create')
def create_subscription():
    form = SubscriptionForm(request.form)
    form.plans.choices = [(plan.plan_id, str(plan)) for plan in service.__retrieve_plans()]

    if form.validate_on_submit():
        fullname = '{} {}'.format(form.card_firstname.data, form.card_lastname.data)
        card = Card(
            form.card_exp_month.data,
            form.card_exp_year.data,
            form.card_number.data,
            form.card_cvc.data,
            fullname)
        try:
            service.register_subscription(card, current_user.get_id(), form.plans.data)
            flash('Subscription created successfully')
            return redirect('/')
        except CardValidationException as e:
            logger.warning('Invalid credit card information: %s', e)
            form.card_number.errors.append('Credit card information does not seem valid')
            return render_template('payments/subscription.html', form=form)
        except PaymentServiceException as e:
            flash('Unexpected error while creating subscription', category='danger')
            logger.error('Unexpected error while creating subscription: %s', e)

    return render_template('payments/subscription.html', form=form)


@blueprint.route('/subscription/cancel')
def cancel_subscription():
    try:
        subscription_id = request.form['subscription_id']
        service.unregister_subscription(subscription_id)
        flash('Subscription cancelled successfully')
        return redirect('/')
    except PaymentServiceException as e:
        flash('Unexpected error while cancelling subscription', category='danger')
        logger.error('Unexpected error while cancelling subscription: %s', e)

    return redirect('/')
