class PaymentServiceException(Exception):
    def __init__(self, cause='N/A'):
        self.cause = cause

    def __repr__(self):
        return '<Cause: {!r}>'.format(self.cause)

    def __str__(self):
        return self.__repr__()


class CardValidationException(PaymentServiceException):
    pass


class CustomerNotFoundException(PaymentServiceException):
    pass
