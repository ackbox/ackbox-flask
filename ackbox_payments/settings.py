from ackbox_core.settings import MetaConfig
from ackbox_payments.datastore_mongoengine import MongoEnginePaymentDatastore
from ackbox_payments.gateway_stripe import StripePaymentService

DEFAULT_STRIPE_CONFIG = {
    'STRIPE_API_KEY': 'sk_test_XbhTxw4SfMkCa2H3GJs4eUS1',
}


class PaymentsCoreConfig(MetaConfig):
    ACKBOX_PAYMENT_DATASTORE_CLASS = MongoEnginePaymentDatastore
    ACKBOX_PAYMENT_GATEWAY_CLASS = StripePaymentService
    ACKBOX_PAYMENT_GATEWAY_CONFIG = DEFAULT_STRIPE_CONFIG
    ACKBOX_PAYMENT_PLANS = []
