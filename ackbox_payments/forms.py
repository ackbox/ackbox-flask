from flask_wtf import FlaskForm
from wtforms import IntegerField
from wtforms import SelectField
from wtforms import StringField
from wtforms import validators


class SubscriptionForm(FlaskForm):
    plans = SelectField('Plans')
    card_firstname = StringField('Cardholder First Name', validators=[
        validators.DataRequired(),
    ])
    card_lastname = StringField('Cardholder Last Name', validators=[
        validators.DataRequired(),
    ])
    card_exp_month = IntegerField('Expiration Month', validators=[
        validators.DataRequired(),
        validators.Length(min=2, max=2),
    ])
    card_exp_year = IntegerField('Expiration Year', validators=[
        validators.DataRequired(),
        validators.Length(min=2, max=2),
    ])
    card_number = IntegerField('Card Number', validators=[
        validators.DataRequired(),
        validators.Length(min=4, max=20),
    ])
    card_cvc = IntegerField('CVC', validators=[
        validators.DataRequired(),
        validators.Length(min=1, max=4),
    ])
    postal_code = IntegerField('Billing Postal Code', validators=[
        validators.DataRequired(),
        validators.Length(max=20),
    ])
