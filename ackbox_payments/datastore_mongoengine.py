from ackbox_payments.datastore import PaymentDatastore
from ackbox_payments.models import Customer
from ackbox_persistence.models import Record
from ackbox_persistence.models import exceptions
from ackbox_persistence.models import fields


class MongoEnginePaymentDatastore(PaymentDatastore):
    def create_customer(self, customer: Customer):
        MongoEngineCustomer(user_id=customer.user_id, customer_id=customer.customer_id).save()

    def retrieve_customer(self, user_id: str) -> Customer:
        customer = MongoEngineCustomer.get_by_user_id(user_id)
        return Customer(user_id=customer.user_id, customer_id=customer.customer_id)


class MongoEngineCustomer(Record):
    user_id = fields.StringField(required=True)
    customer_id = fields.StringField(required=True)

    meta = {'collection': 'customer'}

    @classmethod
    def get_by_user_id(cls, user_id):
        try:
            return cls.objects.get(user_id=user_id)
        except exceptions.DoesNotExist:
            return None

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return '<User {}>'.format(self.username)
