import typing

from ackbox_payments.datastore import PaymentDatastore
from ackbox_payments.exceptions import CustomerNotFoundException
from ackbox_payments.gateway import PaymentGateway
from ackbox_payments.models import Card
from ackbox_payments.models import CardValidation
from ackbox_payments.models import Customer
from ackbox_payments.models import Plan
from ackbox_payments.models import Subscription


class PaymentService(object):
    def __init__(self, gateway: PaymentGateway, datastore: PaymentDatastore):
        self.datastore = datastore
        self.gateway = gateway

    def register_subscription(self, card: Card, user_id: str, plan_id: str) -> Subscription:
        validation_result = self.__validate_card(card)
        self.__create_customer(user_id, validation_result.token)
        return self.__create_subscription(user_id, plan_id)

    def retrieve_subscriptions(self, user_id: str) -> typing.List[Subscription]:
        customer = self.__retrieve_customer(user_id)
        return self.gateway.retrieve_subscriptions(customer.customer_id)

    def unregister_subscription(self, subscription_id: str):
        return self.gateway.cancel_subscription(subscription_id)

    def ensure_plans(self, plans: typing.List[Plan]):
        stored_plans = {plan.plan_id for plan in self.__retrieve_plans()}
        for plan in plans:
            if plan.plan_id not in stored_plans:
                self.__create_plan(plan)

    def __create_subscription(self, user_id: str, plan_id: str) -> Subscription:
        customer = self.__retrieve_customer(user_id)
        if not customer:
            raise CustomerNotFoundException()
        return self.gateway.create_subscription(customer.customer_id, plan_id)

    def __validate_card(self, card: Card) -> CardValidation:
        return self.gateway.validate_card(card)

    def __create_customer(self, user_id: str, source: str) -> Customer:
        created_customer = self.gateway.create_customer(user_id, source)
        self.datastore.create_customer(created_customer)
        return created_customer

    def __retrieve_customer(self, user_id: str) -> Customer:
        return self.datastore.retrieve_customer(user_id)

    def __create_plan(self, plan: Plan) -> Plan:
        return self.gateway.create_plan(plan)

    def __retrieve_plans(self) -> typing.List[Plan]:
        return self.gateway.retrieve_plans()
