import typing

from ackbox_payments.models import Card
from ackbox_payments.models import CardValidation
from ackbox_payments.models import Customer
from ackbox_payments.models import Plan
from ackbox_payments.models import Subscription


class PaymentGateway(object):
    def validate_card(self, card: Card) -> CardValidation:
        raise NotImplemented()

    def create_customer(self, user_id: str, source: str) -> Customer:
        raise NotImplemented()

    def create_plan(self, plan: Plan) -> Plan:
        raise NotImplemented()

    def retrieve_plans(self) -> typing.List[Plan]:
        raise NotImplemented()

    def create_subscription(self, user_id: str, plan_id: str) -> Subscription:
        raise NotImplemented()

    def retrieve_subscriptions(self, user_id: str) -> typing.List[Subscription]:
        raise NotImplemented()

    def cancel_subscription(self, subscription_id: str):
        raise NotImplemented()
