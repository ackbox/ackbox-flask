import time

USD = 'usd'
CAD = 'cad'
MONTH = 'month'
YEAR = 'year'

TIME_INTERVALS = {
    (MONTH, 'Month'),
    (YEAR, 'Year'),
}

CURRENCIES = {
    (USD, 'USD'),
    (CAD, 'CAD'),
}


class Model(object):
    def __repr__(self):
        return '<{!r} {!r}>'.format(self.__class__, self.__dict__)


class BillingAddress(Model):
    def __init__(self,
                 address_city: str,
                 address_country: str,
                 address_line1: str,
                 address_line2: str,
                 address_state: str,
                 address_zip: str):
        self.address_city = address_city
        self.address_country = address_country
        self.address_line1 = address_line1
        self.address_line2 = address_line2
        self.address_state = address_state
        self.address_zip = address_zip


class Plan(Model):
    def __init__(self,
                 plan_id: str,
                 name: str,
                 amount: str,
                 interval: str,
                 trial_period_days: int,
                 currency: str = USD):
        self.plan_id = plan_id
        self.name = name
        self.currency = currency
        self.amount = amount
        self.interval = interval
        self.trial_period_days = trial_period_days
        self.created = time.time()

    def __str__(self):
        return '{} - {} {}'.format(self.name, self.currency, self.amount)


class Customer(Model):
    def __init__(self, user_id: str, customer_id: str):
        self.user_id = user_id
        self.customer_id = customer_id


class Subscription(Model):
    def __init__(self, customer_id: str, plan_id: str, subscription_id: str, status: str):
        self.customer_id = customer_id
        self.plan_id = plan_id
        self.subscription_id = subscription_id
        self.status = status


class Card(Model):
    def __init__(self,
                 exp_month: str,
                 exp_year: str,
                 number: int,
                 cvc: int,
                 fullname: str,
                 currency: str = USD,
                 address: BillingAddress = None):
        self.exp_month = exp_month
        self.exp_year = exp_year
        self.number = number
        self.cvc = cvc
        self.fullname = fullname
        self.currency = currency
        self.address = address


class Token(Model):
    def __init__(self, token_id: str):
        self.token_id = token_id


class CardValidation(Model):
    def __init__(self, card: Card, token: Token):
        self.card = card
        self.token = token
