from flask_mongoengine import MongoEngine

mongoengine_instance = MongoEngine()


class Persistence(object):
    def __init__(self, app=None):
        self.connection = None
        self.db = None
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        mongoengine_instance.init_app(app)

        from mongoengine import connection
        self.connection = connection
        self.db = connection.get_db()
