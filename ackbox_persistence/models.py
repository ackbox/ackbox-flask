from datetime import datetime

import mongoengine as fields
import mongoengine.errors as exceptions

from ackbox_persistence import mongoengine_instance

__all__ = (mongoengine_instance, exceptions, fields)


class Record(mongoengine_instance.Document):
    created_at = fields.DateTimeField(required=True, default=datetime.utcnow())
    meta = {
        'abstract': True
    }

    def update_spec(self, params):
        """ Update an document to match the supplied dictionary.
        """
        for key, value in params.iteritems():
            is_valid = value is not None and value != 'n/a'
            if is_valid and hasattr(self, key):
                setattr(self, key, value)

    def paginated(self, page, items_per_page):
        return paginated(self.objects, page, items_per_page)

    def to_dict(self):
        return self.__dict__

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return '<{} {!r}>'.format(self.__class__.__name__, self.__dict__)


def paginated(cursor, page, items_per_page):
    offset = (page - 1) * items_per_page
    return cursor.skip(offset).limit(items_per_page)
