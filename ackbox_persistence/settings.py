from ackbox_core.settings import MetaConfig


class PersistenceCoreConfig(MetaConfig):
    ACKBOX_MONGODB_SETTINGS = {
        'db': 'ackbox',
        'host': 'localhost',
        'port': 27017,
    }

    @classmethod
    def get_custom_mappings(cls):
        return {
            'MONGODB_SETTINGS': cls.ACKBOX_MONGODB_SETTINGS
        }
