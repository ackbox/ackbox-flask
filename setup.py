import glob
import os

from setuptools import find_packages
from setuptools import setup

version = '0.2.6'

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as file:
    long_description = file.read()


with open(os.path.join(os.path.dirname(__file__), 'requirements.txt')) as file:
    requirements = file.read().split('\n')


def strip_root(filename):
    return '/'.join(filename.split('/')[1:])


def find_package_data(*base_dirs):
    data_files = []
    for base_dir in base_dirs:
        candidates = glob.glob(base_dir, recursive=True)
        data_files += [strip_root(filename) for filename in candidates if os.path.isfile(filename)]
    return data_files


setup(
    name='ackbox-flask',
    version=version,
    packages=find_packages(exclude='env'),
    install_requires=requirements,
    package_data={
        '': ['*.rst'],
        'ackbox_core': find_package_data('ackbox_core/templates/**', 'ackbox_core/static/**'),
        'ackbox_theme': find_package_data('ackbox_theme/templates/**', 'ackbox_theme/static/**'),
    },
    author='Guilherme Trein',
    author_email='trein@ackbox.com',
    description='Ackbox Core Library for Flask-based Application',
    license='MIT',
    url='httpa://www.ackbox.com',
    long_description=long_description,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities',
    ],
)
